#/bin/bash

DEVICES_FILE="devices"
DATA_LOG="devices_log.dat"
MAX_VALUE=85
ITEM=0
FECHA="$(date "+%Y-%m-%d")"
HORA="$(date +'%H:%M:%S')"
FONDO_VERDE="\e[1;42m"
FONDO_ROJO="\e[1;41m"
COLOR_FONDO=$FONDO_VERDE

get_devices(){
    local read_ip
    local read_name
    while IFS=, read -r read_ip read_name; do
        DEVICE_IP+=( "$read_ip" )        
        DEVICE_NAME+=( "$read_name" )
    done < $DEVICES_FILE
}


genera_valor(){
    local max=$1
    local valor=""
    for i in $(seq 1 $max); do
      valor=$valor"."; 
    done
    echo $valor
}


get_devices

for ip in "${DEVICE_IP[@]}"
do
    RSSI=$(curl -s $ip/status | jq '.wifi_sta.rssi')
    #si $R está vacío, no hemos tendido respuesta de Curl (posiblemente off-line)
    if [ -z "$RSSI" ]
    then
        let VALOR=0
        PUNTOS_GRAFICOS=" **OFF-LINE** "
        COLOR_FONDO=$FONDO_ROJO
    else
        let VALOR=$MAX_VALUE+$RSSI
        PUNTOS_GRAFICOS=$(genera_valor $VALOR)
        COLOR_FONDO=$FONDO_VERDE
    fi

    #echo "R=$R\n"
    
    echo -e "$FECHA\t$HORA\t$ip \t ${DEVICE_NAME[$ITEM]} \t$RSSI\t $COLOR_FONDO$PUNTOS_GRAFICOS\e[0m"
    echo -e "$FECHA\t$HORA\t$ip\t${DEVICE_NAME[$ITEM]}\t$RSSI\t$VALOR" >> $DATA_LOG
    ITEM=$(( $ITEM + 1 ))
done


