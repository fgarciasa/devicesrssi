# DevicesRssi

Display Devices RSSI on local network

This script scans shelly devices on local network and display a graphic with RSSI data

The devices to be scanned are in *devices* file

The format of this file is: ip,device name

Also, the script adds all data to *devices.log* file: Date, Time, IP, device Name and RSSI value.


![Graphic Result](rssi_graphic.png)


